# Илья Катушенок

## Volunteer contributor of Geometry Nodes
I want to be able to fully procedurally generate rigged character with full body physics simulation of bones, muscles, fat and hairs.
Build all outfit from drawn 2d cutting fabric with fully procedural textures with different patterns and text labels.
All of this should be rendered by as large shaders of different noises and color mixtures as so GPU will build this at least at few minutes.
This characters should be simulated in city with at least 1000+ residents and never colis with other.
Atmosphere should include as car and people smoke and factories as well. Clouds need to be able to be down to create fog.
Trees have to result of evolution simulation while 1000+ years.
And all of that in just 5 sec of bakes.
So, there is some works to do. Oh, and of course solid models and CAD-like surfaces of everything.

## Volunteer of bug triaging module


### System Information
- Operating system: Windows-10-10.0.19045-SP0 64 Bits
- Graphics card: NVIDIA GeForce GTX 1060 6GB/PCIe/SSE2 NVIDIA Corporation 4.6.0 NVIDIA 527.56

## Contacts:
 - Blender.chat: [Iliya Katushenock](https://blender.chat/direct/mod).
 - Devtalk: [MOD (Iliay Katushenock)](https://devtalk.blender.org/u/modmodervaaaa).